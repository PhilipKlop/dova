journeysim.jar is de simulator voor KV6-berichten

xml-bison.jar bevat de jaxb classes voor KV6 (en andere koppelvlakken, maar die zijn hier niet relevant). Een ontvangen KV6-xml zou van het type nl.connekt.bison.tmi8.kv6.msg.VVTMPUSH moeten zijn.