import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.Message;

public class VehicleJourney extends AbstractVerticle {

    private final VehicleJourneyId id;
    private final Vertx vertx;
    private final Future<String> onComplete;
    private long timerId = -1;

    public VehicleJourney(VehicleJourneyId id, Vertx vertx, Future<String> onComplete) {
        this.id = id;
        this.vertx = vertx;
        this.onComplete = onComplete;
    }

    @Override
    public void start() {
        vertx.eventBus().consumer(id.toString(), this::handleUpdate);
        resetTimer();
        System.out.println("VehicleJourney deployed with id: " + id.toString());
    }

    private void handleUpdate(Message<String> message) {
        resetTimer();
        String messageType = message.body();
        System.out.println("VehicleJourney received update: " + messageType);
        if (messageType.equals("ENDType")) {
            onComplete.complete(id.toString());
        }
    }

    private void resetTimer() {
        if (timerId > -1) {
            vertx.cancelTimer(timerId);
        }
        timerId = vertx.setTimer(60 * 1000, t -> this.onComplete.complete(id.toString()));
    }
}
