public class Instruction {
    private final VehicleJourneyId veJoId;
    private final String message;

    public Instruction(VehicleJourneyId veJoId, String message) {
        this.veJoId = veJoId;
        this.message = message;
    }

    public VehicleJourneyId getVeJoId() {
        return veJoId;
    }

    public String getMessage() {
        return message;
    }
}
