import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;
import io.vertx.core.http.HttpServerResponse;
import nl.connekt.bison.tmi8.kv6.msg.KV6PosinfoType;
import nl.connekt.bison.tmi8.kv6.msg.VVTMPUSH;

import java.util.HashMap;
import java.util.Map;

public class DovaListener {

    private final Map<String, VehicleJourney> vehicleJourneys;
    private final Map<String, String> deploymentIds;
    private final Vertx vertx = Vertx.vertx();

    public DovaListener() {
        vehicleJourneys = new HashMap<>();
        deploymentIds = new HashMap<>();
    }

    public void start() {
        HttpServer server = vertx.createHttpServer();
        server.requestHandler(this::handleRequest);
        server.exceptionHandler(this::handleError);
        server.listen(8080);
    }

    private void handleError(Throwable error) {
        System.out.println("Received error: " + error.toString());
    }

    private void handleRequest(HttpServerRequest request) {
        final HttpServerResponse response = request.response();
        request.bodyHandler(totalBuffer -> {
            String source = totalBuffer.toString();
            VVTMPUSH message = ConvertXml.<VVTMPUSH>fromString(source).toInstanceOf(VVTMPUSH.class);
            message.getKV6Posinfo().forEach(this::processPosInfo);
            response.end();
        });
    }

    private void processPosInfo(KV6PosinfoType info) {
        info.getDELAYAndINITAndARRIVAL().stream()
                .map(InstructionUtil::convertToInstruction)
                .forEach(this::processInstruction);
    }

    private void processInstruction(Instruction instruction) {
        VehicleJourneyId id = instruction.getVeJoId();
        if (vehicleJourneys.get(id.toString()) == null) {
            createAndDeployVehicleJourney(id);
        } else {
            vertx.eventBus().publish(id.toString(), instruction.getMessage());
        }
    }

    private void createAndDeployVehicleJourney(VehicleJourneyId id) {
        Future<String> future = Future.future();
        future.setHandler(this::undeployVehicleJourney);
        VehicleJourney journey = new VehicleJourney(id, vertx, future);
        vertx.deployVerticle(journey, res -> {
            if (res.succeeded()) {
                deploymentIds.put(id.toString(), res.result());
            }
        });
        vehicleJourneys.put(id.toString(), journey);
        System.out.println("Number of deployed vehicle journeys: " + vehicleJourneys.size());
    }

    private void undeployVehicleJourney(AsyncResult<String> result) {
        if (result.succeeded()) {
            String id = result.result();
            System.out.println("Undeploying verticle with id: " + id);
            String deploymentId = deploymentIds.get(id);
            vertx.undeploy(deploymentId);
            vehicleJourneys.remove(id);
            deploymentIds.remove(id);
        }
    }
}
