import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.StringReader;
import java.nio.file.Path;
import java.util.function.Function;

public class ConvertXml {

    public static <T> ClassCaster<T> fromFile(Path path) {
        return convert(parseFunction(path));
    }

    public static <T> ClassCaster<T> fromString(String string) {
        return convert(parseFunction(string));
    }

    public static <T> ClassCaster<T> convert(Function<Unmarshaller, Object> parseFunction) {
        return targetClass -> {
            try {
                final JAXBContext context = JAXBContext.newInstance(targetClass);
                final Unmarshaller unmarshaller = context.createUnmarshaller();
                final Object object = parseFunction.apply(unmarshaller);
                return targetClass.cast(object);
            } catch (final JAXBException exception) {
                throw new XmlParseException(exception);
            }
        };
    }

    private static Function<Unmarshaller, Object> parseFunction(Path path) {
        return u -> {
            try {
                return u.unmarshal(path.toFile());
            } catch (JAXBException e) {
                throw new XmlParseException(e);
            }
        };
    }

    private static Function<Unmarshaller, Object> parseFunction(String string) {
        return u -> {
            try {
                return u.unmarshal(new StringReader(string));
            } catch (JAXBException e) {
                throw new XmlParseException(e);
            }
        };
    }

    @FunctionalInterface
    public interface ClassCaster<T> {
        T toInstanceOf(Class<T> targetClass);
    }

    private static class XmlParseException extends RuntimeException {
        XmlParseException(Throwable cause) {
            super(cause);
        }
    }
}
