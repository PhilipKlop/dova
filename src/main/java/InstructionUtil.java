import nl.connekt.bison.tmi8.kv6.msg.ARRIVALType;
import nl.connekt.bison.tmi8.kv6.msg.DELAYType;
import nl.connekt.bison.tmi8.kv6.msg.DEPARTUREType;
import nl.connekt.bison.tmi8.kv6.msg.ENDType;
import nl.connekt.bison.tmi8.kv6.msg.INITType;
import nl.connekt.bison.tmi8.kv6.msg.OFFROUTEType;
import nl.connekt.bison.tmi8.kv6.msg.ONPATHType;
import nl.connekt.bison.tmi8.kv6.msg.ONROUTEType;
import nl.connekt.bison.tmi8.kv6.msg.ONSTOPType;

public class InstructionUtil {
    public static Instruction convertToInstruction(Object o) {
        Instruction result;
        String clazz = o.getClass().getSimpleName();
        switch (clazz) {
            case "DELAYType":
                result = createInstruction((DELAYType) o);
                break;
            case "INITType":
                result = createInstruction((INITType) o);
                break;
            case "ARRIVALType":
                result = createInstruction((ARRIVALType) o);
                break;
            case "ONSTOPType":
                result = createInstruction((ONSTOPType) o);
                break;
            case "DEPARTUREType":
                result = createInstruction((DEPARTUREType) o);
                break;
            case "ONROUTEType":
                result = createInstruction((ONROUTEType) o);
                break;
            case "ONPATHType":
                result = createInstruction((ONPATHType) o);
                break;
            case "OFFROUTEType":
                result = createInstruction((OFFROUTEType) o);
                break;
            case "ENDType":
                result = createInstruction((ENDType) o);
                break;
            default:
                result = null;
        }
        return result;
    }

    private static Instruction createInstruction(ENDType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(OFFROUTEType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(ONPATHType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(ONROUTEType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(DEPARTUREType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(ONSTOPType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(ARRIVALType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(INITType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }

    private static Instruction createInstruction(DELAYType o) {
        VehicleJourneyId id = new VehicleJourneyId(o.getDataownercode(), o.getLineplanningnumber(), o.getOperatingday(), o.getJourneynumber(), o.getReinforcementnumber());
        return new Instruction(id, o.getClass().getSimpleName());
    }
}
