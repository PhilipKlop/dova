import nl.connekt.bison.tmi8.kv6.msg.INITType;

import javax.xml.datatype.XMLGregorianCalendar;
import java.util.Objects;

public class VehicleJourneyId {

    private final String dataownercode;
    private final String lineplanningnumber;
    private final XMLGregorianCalendar operatingday;
    private final int journeynumber;
    private final int reinforcementnumber;

    public VehicleJourneyId(String dataownercode, String lineplanningnumber, XMLGregorianCalendar operatingday, int journeynumber, int reinforcementnumber) {
        this.dataownercode = dataownercode;
        this.lineplanningnumber = lineplanningnumber;
        this.operatingday = operatingday;
        this.journeynumber = journeynumber;
        this.reinforcementnumber = reinforcementnumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleJourneyId vehicleJourneyId = (VehicleJourneyId) o;
        return journeynumber == vehicleJourneyId.journeynumber &&
                reinforcementnumber == vehicleJourneyId.reinforcementnumber &&
                Objects.equals(dataownercode, vehicleJourneyId.dataownercode) &&
                Objects.equals(lineplanningnumber, vehicleJourneyId.lineplanningnumber) &&
                Objects.equals(operatingday, vehicleJourneyId.operatingday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dataownercode, lineplanningnumber, operatingday, journeynumber, reinforcementnumber);
    }

    @Override
    public String toString() {
        return "VehicleJourneyId{" +
                "dataownercode='" + dataownercode + '\'' +
                ", lineplanningnumber='" + lineplanningnumber + '\'' +
                ", operatingday=" + operatingday +
                ", journeynumber=" + journeynumber +
                ", reinforcementnumber=" + reinforcementnumber +
                '}';
    }

    public static VehicleJourneyId fromInitType(INITType input) {
        return new VehicleJourneyId(input.getDataownercode(), input.getLineplanningnumber(),
                input.getOperatingday(), input.getJourneynumber(), input.getReinforcementnumber());
    }


}
